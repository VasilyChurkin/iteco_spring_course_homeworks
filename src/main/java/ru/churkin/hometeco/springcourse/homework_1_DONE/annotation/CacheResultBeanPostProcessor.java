package ru.churkin.hometeco.springcourse.homework_1_DONE.annotation;

import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class CacheResultBeanPostProcessor implements BeanPostProcessor {

    // Мапа для кэширования
    private Map<String, Map<String, Object>> cacheMap = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        log.info("Caching..................");
        Method[] methods = bean.getClass().getMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(CacheResult.class)) {
                CacheResult annotation = method.getAnnotation(CacheResult.class);
                String value = annotation.value();
                ProxyFactory proxyFactory = new ProxyFactory(bean);
                log.info("Before call method in ExternalService");
                proxyFactory.addAdvice((MethodInterceptor) invocation -> {
                    Object[] keys = invocation.getArguments();
                    // ключ для хранения в кэше - составная строка из аргументов метода с разделителем _
                    String key = Arrays.stream(keys)
                            .map(Object::toString)
                            .collect(Collectors.joining("_"));
                    // проверка наличия элемента в кэше
                    if (cacheMap.get(value) != null && cacheMap.get(value).get(key) != null) {
                        Object cacheResult = cacheMap.get(value).get(key);
                        log.info("Get from cache for cache-value: {}, key:{}, result: {}", value, key, cacheResult);
                        return cacheResult;
                    }
                    Object proceed = invocation.proceed();
                    return fillCache(proceed, value, key);
                });
                return proxyFactory.getProxy();
            }
        }
        return bean;
    }

    private Object fillCache(Object proceed, String value, String key) {
        if (cacheMap.get(value) == null) {
            cacheMap.put(value, new HashMap<>());
            log.info("creating new value in CACHE");
        }
        cacheMap.get(value).put(key, proceed);
        log.info("put new key in CACHE for value: {}", value);
        return proceed;
    }
}
