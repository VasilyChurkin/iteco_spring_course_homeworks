package ru.churkin.hometeco.springcourse.homework_1_DONE.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;
import ru.churkin.hometeco.springcourse.homework_1_DONE.service.Process;

@Slf4j
@Component
@Lazy
public class ExternalInfoProcess implements Process {

    @Value("${id-not-process}")
    private Integer idNotProcess;

    @Override
    public boolean run(ExternalInfo externalInfo) {
        log.info("Read property \"id-not-process\" with value: \"{}\"", idNotProcess);
        if (externalInfo.getId().equals(idNotProcess)) {
            log.info("External info ID equals id-not-process");
            return true;
        }
        log.info("External info ID is \"{}\", but id-not-process is \"{}\"", externalInfo.getId(), idNotProcess);
        return false;
    }

}
