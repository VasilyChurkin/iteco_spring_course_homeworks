package ru.churkin.hometeco.springcourse.homework_1_DONE.service;

import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;

public interface ExternalService {

    ExternalInfo getExternalInfo(Integer id);

    ExternalInfo readExternalInfo(Integer id);

}
