package ru.churkin.hometeco.springcourse.homework_1_DONE.annotation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Slf4j
@Component
public class CachePrototypeBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        log.info("Controlling CacheResult for Prototypes...");
        boolean result = false;
        if (bean.getClass().isAnnotationPresent(Scope.class)) {
            String scopeName = bean.getClass().getAnnotation(Scope.class).value();
            if ("prototype".equals(scopeName)) {
                Method[] declaredMethods = bean.getClass().getDeclaredMethods();
                for (Method method : declaredMethods) {
                    if (method.isAnnotationPresent(CacheResult.class)) {
                        log.warn("Bean {} has scope: prototype and CacheResult annotation", beanName);
                    }
                }
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
