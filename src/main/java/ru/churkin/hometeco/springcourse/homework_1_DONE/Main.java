package ru.churkin.hometeco.springcourse.homework_1_DONE;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import ru.churkin.hometeco.springcourse.homework_2.service.ExternalService;

@Slf4j
@ComponentScan
@EnableCaching
@PropertySource("classpath:application.properties")
public class Main {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(Main.class);

        // смотрим работу кэша: при первом обращении идет в метод, при втором вытасквает из кеша
        ExternalService externalService = context.getBean(ExternalService.class);
        log.info(externalService.getExternalInfo(2).toString());
        log.info(externalService.getExternalInfo(1).toString());
        log.info(externalService.getExternalInfo(1).toString());
        log.info(externalService.getExternalInfo(2).toString());

        // здесь тоже идет обращение в тот же кеш
        /*log.info(externalService.readExternalInfo(1).toString());
        log.info(externalService.readExternalInfo(2).toString());
        log.info(externalService.readExternalInfo(1).toString());
        log.info(externalService.readExternalInfo(2).toString());*/

        // проверка вычитки id-not-process
        /*Process process = context.getBean(Process.class);
        process.run(new ExternalInfo(3, "test-info"));*/

        // проверка Flow
//        Flow flow = context.getBean(Flow.class);
//        flow.run(1);
//        flow.run(2);
//        flow.run(2);
//        flow.run(3);
//        flow.run(4);


        // проврка аннотации CachedResult
//        externalService.getExternalInfo(1);


        // смотрим как очищается мапа с тестовым данными при очистке контекста
        /*((AnnotationConfigApplicationContext) context).close();*/

    }
}
