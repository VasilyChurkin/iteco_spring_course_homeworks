package ru.churkin.hometeco.springcourse.homework_1_DONE.service;

import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;

public interface Process {

    boolean run(ExternalInfo externalInfo);

}
