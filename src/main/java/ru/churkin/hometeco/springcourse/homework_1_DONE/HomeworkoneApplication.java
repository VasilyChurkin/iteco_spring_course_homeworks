package ru.churkin.hometeco.springcourse.homework_1_DONE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkoneApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeworkoneApplication.class, args);
    }

}
