package ru.churkin.hometeco.springcourse.homework_2.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.churkin.hometeco.springcourse.homework_2.annotation.CacheResult;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Aspect
@Component
@Slf4j
@Order(1)
public class AspectCacheResult {

    // Мапа для кэширования
    private Map<String, Map<String, Object>> cacheMap = new HashMap<>();

    @Pointcut(value="@annotation(ru.churkin.hometeco.springcourse.homework_2.annotation.CacheResult)")
    public void getMethodCacheResult(){
    }

    /**
     * 4 - Аспект для реализации кеша из первого ДЗ.
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around(value = "getMethodCacheResult()")
    public Object cache(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("CacheResult from Aspect");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        CacheResult annotation = method.getAnnotation(CacheResult.class);
        String value = annotation.value();
        final Object[] keys = joinPoint.getArgs();
        String key = Arrays.stream(keys)
                .map(Object::toString)
                .collect(Collectors.joining("_"));
        if (cacheMap.get(value) != null && cacheMap.get(value).get(key) != null) {
            Object cacheResult = cacheMap.get(value).get(key);
            log.info("Get from cache for cache-value: {}, key:{}, result: {}", value, key, cacheResult);
            return cacheResult;
        }
        Object proceed = joinPoint.proceed();
        return fillCache(proceed, value, key);
    }

    private Object fillCache(Object proceed, String value, String key) {
        if (cacheMap.get(value) == null) {
            cacheMap.put(value, new HashMap<>());
            log.info("creating new value in CACHE");
        }
        cacheMap.get(value).put(key, proceed);
        log.info("put new key in CACHE for value: {}", value);
        return proceed;
    }
}

