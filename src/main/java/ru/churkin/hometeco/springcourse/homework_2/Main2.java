package ru.churkin.hometeco.springcourse.homework_2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;
import ru.churkin.hometeco.springcourse.homework_2.service.ExternalService;
import ru.churkin.hometeco.springcourse.homework_2.service.Process;

@Slf4j
@ComponentScan
@EnableAspectJAutoProxy
@PropertySource("classpath:application.properties")
public class Main2 {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(Main2.class);

        // смотрим работу кэша: при первом обращении идет в метод, при втором вытасквает из кеша
        // Работает аспект AspectCacheResult
        ExternalService externalService = context.getBean(ExternalService.class);
//        externalService.getExternalInfo(2);
//        externalService.getExternalInfo(2);
//        externalService.getExternalInfo(1);
//        externalService.getExternalInfo(1);
//        externalService.getExternalInfo(2);

        // здесь тоже идет обращение в тот же кеш
        /*externalService.readExternalInfo(1);
        externalService.readExternalInfo(2);
        externalService.readExternalInfo(1);
        externalService.readExternalInfo(2);*/

        // проверка работы аспектов для метода с исключением (использование @AfterThrowable) - проверялось при реализаци аспектов 1 и 2
        // Проверка работы аспекта AspectCheckRequest
        Process process = context.getBean(Process.class);
        process.run(new ExternalInfo(3, "test-info"));
        process.run(new ExternalInfo(1, "test-info-1"));

    }
}
