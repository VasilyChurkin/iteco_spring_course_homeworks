package ru.churkin.hometeco.springcourse.homework_2.service;

import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;

public interface Process {

    Boolean run(ExternalInfo externalInfo);

}
