package ru.churkin.hometeco.springcourse.homework_2.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.churkin.hometeco.springcourse.homework_2.annotation.CacheResult;
import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;
import ru.churkin.hometeco.springcourse.homework_2.service.ExternalService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
//@Scope("prototype")
public class ExternalServiceImpl implements ExternalService {

    private static final Map<Integer, ExternalInfo> testData = new HashMap<>();

    @Override
    @CacheResult(value = "externalInfo")
    public ExternalInfo getExternalInfo(Integer id) {
        log.info("Get ExternalInfo by id={}", id);
        ExternalInfo result = testData.get(id);
        if (result == null) {
            log.error("\"Not found value with id={}", id);
            throw new RuntimeException("Not found!");
        }
        log.info("ExternalInfo for id={} is {}", id, result);
        return result;
    }

    /// какой-то другой метод, для которого кеширование тоже работает
    @CacheResult(value = "externalInfo")
    public ExternalInfo readExternalInfo(Integer id) {
        log.info("Get ExternalInfo by id={}", id);
        return testData.get(id);
    }

    @PostConstruct
    public void fillMap(){
        log.info("Fill testData in post Construct");
        testData.put(1, new ExternalInfo(1, null));
        testData.put(2, new ExternalInfo(2, "hasInfo"));
        testData.put(3, new ExternalInfo(3,"info"));
        testData.put(4, new ExternalInfo(4, "information"));
        testData.forEach((Integer id, ExternalInfo info)-> log.info("New testData was added in TestData ({}:{})", id, info.toString()));
        log.info("TestFata was filled successfully. Result size = {} ", testData.size());
    }

    @PreDestroy
    public void cleanMap(){
        log.info("Start clean testData...");
        testData.clear();
        if (!(testData.size() == 0)) {
            log.error("TestData wasn't clean");
        };
        log.info("Testdata was clean successfully. Result size = {} ", testData.size());
    }
}
