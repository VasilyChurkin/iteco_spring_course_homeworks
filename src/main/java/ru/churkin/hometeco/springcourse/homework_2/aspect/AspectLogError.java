package ru.churkin.hometeco.springcourse.homework_2.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Aspect
@Order(1)
public class AspectLogError {

    /**
     * 2-Логирует ошибки, возникшие при работе всех методов.
     *
     * @param joinPoint
     * @param e         - отбрасываемое исклчение
     */
    @AfterThrowing(value = "getAllMethods()", throwing = "e")
    public void logExceptions(JoinPoint joinPoint, Exception e) {
        log.error("Method {} execute with exception: {}", joinPoint.getSignature().toShortString(), e.toString());
    }

    // при использовании закомментированного пойнтката отбрасывается исключение
    // Caused by: java.lang.IllegalArgumentException: Cannot subclass final class org.springframework.boot.autoconfigure.AutoConfigurationPackages$BasePackages
    // оставил "для всех методов из проекта"
//        @Pointcut(value="(execution(* *(..)))")
    @Pointcut(value = "execution(* ru.churkin.hometeco..*(..))")
    public void getAllMethods() {
    }

}
