package ru.churkin.hometeco.springcourse.homework_2.service;

import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;

public interface ExternalService {

    ExternalInfo getExternalInfo(Integer id);

    ExternalInfo readExternalInfo(Integer id);

}
