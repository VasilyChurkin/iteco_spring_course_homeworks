package ru.churkin.hometeco.springcourse.homework_2.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
@Aspect
@Order(1)
public class AspectCheckRequest {

    @Value("${id-not-process}")
    private Integer idNotProcess;

    @Pointcut(value = "@annotation(ru.churkin.hometeco.springcourse.homework_2.annotation.CheckRequest)")
    public void getMethodWithCheckResult() {
    }

    /**
     * 3 - При аннотировании метода аннотацией CheckRequest и наличии в аргументах метода ExternalInfo необходимо
     * выполнить проверку, если ExternalInfo.getId == config.id-not-process, то основной метод не вызываем,
     * иначе передаем управление основному методу.
     * @param joinPoint
     */
    @Around(value = "getMethodWithCheckResult()")
    public void getCheckRequest(ProceedingJoinPoint joinPoint) {
        Object[] allArgs = joinPoint.getArgs();
        List<ExternalInfo> externalInfoArgs = filterTypes(allArgs);
        for (ExternalInfo arg : externalInfoArgs) {
            if (arg.getId() != idNotProcess) {
                try {
                    log.info("Proceeding...");
                    joinPoint.proceed();
                }
                catch(Throwable t) {
                   log.error("Id for ExternalInfo in method {} is in list id-not-process", joinPoint.toShortString());
                }
            }
        }
    }

    private List<ExternalInfo> filterTypes(Object[] args) {
        List<ExternalInfo> result = Arrays.stream(args)
                .filter(arg -> ExternalInfo.class.isAssignableFrom(arg.getClass()))
                .map(e -> (ExternalInfo) e)
                .collect(Collectors.toList());
        return result;
    }

}
