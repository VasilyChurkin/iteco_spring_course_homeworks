package ru.churkin.hometeco.springcourse.homework_2.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ru.churkin.hometeco.springcourse.homework_2.model.ExternalInfo;
import ru.churkin.hometeco.springcourse.homework_2.service.impl.ExternalInfoProcess;

@Slf4j
@Component
//@RequiredArgsConstructor
public class Flow {

    private final ExternalService externalService;
    private final ExternalInfoProcess externalInfoProcess;

    public Flow(ExternalService externalService,
                @Lazy ExternalInfoProcess externalInfoProcess) {
        this.externalService = externalService;
        this.externalInfoProcess = externalInfoProcess;
    }

    public void run(Integer id) {
        log.info("Start Flow#run() with id={}", id);
        ExternalInfo externalInfo = externalService.getExternalInfo(id);
        log.info("ExternalInfo was search: {}", externalInfo.toString());
//        if (Objects.isNull(externalInfo.getInfo())) {
        if (externalInfo.getInfo() != null) {
            externalInfoProcess.run(externalInfo);
        }
        log.info("Other situation. {}", externalInfoProcess.getClass());
     }


}
