package ru.churkin.hometeco.springcourse.homework_2.aspect;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Aspect
@Order(0)
public class AspectEnterExit {

    /**
     * 1-Логирует начало и конец работы всех методов из пакета service.
     */
    @Around("getAllMethodsInsideServicePackage()")
    public void aroundAllMethodAdvice(ProceedingJoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().toShortString();
        log.info("Start execution method: {}", methodName);
        try {
            joinPoint.proceed();
            log.info("Method {} execute with success", methodName);
        } catch (Throwable throwable) {
            log.error("Method {} exit with exception!", methodName);
            throwable.printStackTrace();
        }
    }

    @Pointcut(value = "within(ru.churkin.hometeco.springcourse.homework_2.service..*)")
//    @Pointcut(value = "execution(* *..service..*(..))")
    public void getAllMethodsInsideServicePackage(){
    }
}
